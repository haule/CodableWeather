//
//  WeatherData.swift
//  CodableWeather
//
//  Created by Blake Laing on 7/6/18.
//  Copyright © 2018 Blake Laing. All rights reserved.
//

import Foundation

class WeatherData {
    //Determine these two parameters from data pulled from Wunderground.com, OpenWeatherMap.org or similar using lattitude/longitude
    var stationID : String
    var stationElevation : Double


    var temperatureC: Double
    var temperatureDewpointC: Double
    var pressureMb: Double
    var stationRho : Double

    var windSpeed: Double
    var windDegrees: UInt
    var windString: String

    
    init() {
        stationID = ""
        stationElevation = 0.0
        
        temperatureC = 0.0
        temperatureDewpointC = 0.0
        pressureMb = 0.0
        stationRho = 0.0
        
        windSpeed = 0.0
        windDegrees = 0
        windString = ""
    }
}



