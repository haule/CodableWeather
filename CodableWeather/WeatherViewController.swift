//
//  ViewController.swift
//  CodableWeather
//
//  Created by Blake Laing on 7/6/18.
//  Copyright © 2018 Blake Laing. All rights reserved.
//

import UIKit


import UIKit
import CoreLocation
import Alamofire
import SwiftyJSON


class WeatherViewController: UIViewController {


    let weatherData : WeatherData = WeatherData()
    
    //MARK - pull weather data from wunderground
    let WEATHER_BASE_URL = "https://api.wunderground.com/api/9cd4ed2852b10c25/conditions/lang:EN/q/"
    var wUndergroundQuery = "35.0531,-85.0502"
    var WEATHER_URL : String = ""
    
    @IBAction func buttonCheckWeather(_ sender: Any) {
        WEATHER_URL = WEATHER_BASE_URL + wUndergroundQuery + ".json"
        print("query:", WEATHER_URL)
        getWeatherData(url: WEATHER_URL)

    }
    @IBOutlet weak var dispStationID: UILabel!
    @IBOutlet weak var dispTemperatureC: UILabel!
    
    //MARK: - Networking
    /***************************************************************/
    //Write the getWeatherData method here:
    func getWeatherData(url: String) {
        print("sending Alamofire request")
        Alamofire.request(url, method: .get).responseJSON {
            response in
            if response.result.isSuccess {
                
                print("Success! Got the weather data")
                let weatherJSON : JSON = JSON(response.result.value!)
                
                print(weatherJSON)
                
                self.updateWeatherData(json: weatherJSON)
                self.dispStationID.text = self.weatherData.stationID
                self.dispTemperatureC.text = self.weatherData.temperatureC as? String
            }
            else {
                print("Error with Alamofire \(String(describing: response.result.error))")
                //                self.cityLabel.text = "Connection Issues"
            }
        }
        
    }
    
    //MARK: - JSON Parsing (I'd rather this be done with Codable, not using Alamofire
    /***************************************************************/

    func updateWeatherData(json : JSON) {
        var stationID : String
        var stationElevation : Double
        
        
        var temperatureC: Double
        var temperatureDewpointC: Double
        var pressureMb: Double
        var stationRho : Double
        
        var windSpeed: Double
        var windDegrees: UInt
        var windString: String
        
        weatherData.stationID = json["current_observation"]["station_id"].stringValue
        weatherData.stationElevation = json["current_observation"]["display_location"]["elevation"].doubleValue
        
        weatherData.temperatureC =  json["current_observation"]["temp_c"].doubleValue
        weatherData.temperatureDewpointC = json["current_observation"]["dewpoint_c"].doubleValue
        weatherData.pressureMb = json["current_observation"]["pressure_mb"].doubleValue
        //I can calculate stationRho from the above
        
        weatherData.windSpeed = json["current_observation"]["wind_kph"].doubleValue
        weatherData.windDegrees = json["current_observation"]["wind_degrees"].uIntValue
        weatherData.windString = json["current_observation"]["wind_string"].stringValue
    }
    
    
}

